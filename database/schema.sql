BEGIN TRANSACTION;
CREATE table Addresses(
  AddressId INTEGER PRIMARY KEY,
  BuildingName TEXT,
  StreetNumber INTEGER,
  Street TEXT,
  City TEXT,
  State TEXT,
  ZipCode INTEGER,
  PlusFour INTEGER
);
CREATE table Clients(
  ShortName TEXT PRIMARY KEY,
  FullName TEXT,
  AddressId INTEGER,
  Email TEXT,
  Phone TEXT
);
CREATE table Services(
  ServcieId INTEGER PRIMARY KEY,
  Price INTEGER,
  RenewalMonth INTEGER,
  RenewalDay INTEGER,
  Description TEXT,
  Clientid INTEGER
);
CREATE table Jobs(
  JobId INTEGER PRIMARY KEY,
  Rate INTEGER,
  Hours REAL,
  DateComplete TEXT,
  Description TEXT,
  ClientId INTEGER
);
CREATE table ServiceCharges(
  Year INTEGER,
  ServiceId INTEGER,
  Processed INTEGER
);
COMMIT;
